const N: usize = 3;
const SEND_EVERY:usize = 10;

struct WorldParams{
    gamma: f64,
    mu: f64,
    a_t: f64,
}

enum Message{
    TaskMessage(String),
    ParameterMessage(f64, f64),
    ProgressMessage(f64),
    DoneMessage(),
    ResultMessage(f64, f64, [(f64, f64, f64); 3]),
}

mod lattice {

    #[derive(Clone)]
    struct LatticeSites{
        len: usize,
        member_of_baryon_loops: Vec<bool>,
        ks: Vec<Vec<[usize; 2]>>,
        bs: Vec<Vec<[isize; 2]>>,
    }

    struct Lattice{
        dimensions: usize,
        shape: Vec<usize>,
        strides: Vec<usize>,
        volume: usize,
        sites: LatticeSites,
    }

    fn get_coord_from_index(shape: &Vec<usize>, index: usize, direction: usize) -> usize{
        let mut coord = 0;
        {
            let mut index = index;
            for extent in &shape[0..direction+1]{
                coord = index % extent;
                index -= coord;
                index /= extent;
            }
        }
        coord
    }

    fn new_index_from_move(shape: &Vec<usize>, strides: &Vec<usize>, index: usize, direction: usize, step: usize) -> usize{
        let coord = get_coord_from_index(shape, index, direction);

        if step == 0{
            if coord == 0{
                return index + (shape[direction] - 1) * strides[direction]
            }else{
                return index - strides[direction]
            }
        }else{
            if coord == shape[direction] - 1{
                index - (shape[direction] - 1) * strides[direction]
            }else{
                index + strides[direction]
            }
        }
    }

    fn check_winding(shape: &Vec<usize>, index: usize, direction: usize, step: usize) -> bool{
        let coord = get_coord_from_index(shape, index, direction);
        if step == 0{
            coord == 0
        }else{
            coord == shape[direction] - 1
        }
    }


    fn init_lattice(shape: Vec<usize>) -> Lattice {
        let mut amount_lattice_sites = 1;
        let mut strides: Vec<usize> = vec!(1);
        for extent in &shape{
            amount_lattice_sites *= extent;
            strides.push(amount_lattice_sites);
        }

        let mut sites = LatticeSites{
            len: amount_lattice_sites,
            member_of_baryon_loops: Vec::new(),
            ks: Vec::new(),
            bs: Vec::new(),
        };

        for index in 0..amount_lattice_sites{
            sites.member_of_baryon_loops.push(false);
            let mesonic_connection = [0, 0];
            let mut mesonic_connections: Vec<[usize; 2]> = Vec::new();
            let baryonic_connection = [0, 0];
            let mut baryonic_connections: Vec<[isize; 2]> = Vec::new();
            for _ in 0..shape.len(){
                mesonic_connections.push(mesonic_connection);
                baryonic_connections.push(baryonic_connection);
            }
            sites.ks.push(mesonic_connections.clone());
            sites.bs.push(baryonic_connections.clone());
            if index % 2 == 0 {
                sites.ks[index][0][0] = 1;
                sites.ks[index][0][1] = 2;
            }else{
                sites.ks[index][0][0] = 2;
                sites.ks[index][0][1] = 1;
            }
        }

        let mut volume = 1;
        for extent in &shape[1..]{
            volume *= extent;
        }


        Lattice{
            dimensions: shape.len(),
            shape: shape.clone(),
            strides: strides.clone(),
            volume,
            sites,
        }

    }


    pub mod worm{
        use rand::prelude::*;
        use super::Lattice;
        use crate::WorldParams;
        use crate::N;
        use super::new_index_from_move;
        const MESON_WORM_STEP1_TRIES: usize = 100;
        const BARYON_WORM_STEP1_TRIES: usize = 100;
        pub(super) fn meson_worm(lattice: &mut Lattice, params: &WorldParams, rng: &mut impl Rng) -> bool{
            let result = meson_worm_step1(lattice, rng);
            let (starting_index, mut result) = match result {
                None => {return false;}
                Some(x) => x,
            };
            loop{
                result = meson_worm_step2(lattice, params, result, rng);
                let done: bool;
                (done, result) = meson_worm_step3(lattice, result, starting_index, rng);
                if done {
                    return true;
                }
            }
        }

        fn meson_worm_step1(lattice: &mut Lattice, rng: &mut impl Rng) -> Option<(usize, (usize, usize, usize))> {
            let mut site_index;
            for _ in 0..MESON_WORM_STEP1_TRIES {
                let mut tries = 0;
                loop {
                    site_index = rng.gen_range(0..lattice.sites.len);
                    if !lattice.sites.member_of_baryon_loops[site_index] {
                        break;
                    }
                    tries += 1;
                    if tries > MESON_WORM_STEP1_TRIES {
                        return None;
                    }
                }

                let correction_to_probability = 1.0;

                for nu in 0..lattice.dimensions {
                    for forward in [0,1]{
                        let p_nu = (lattice.sites.ks[site_index][nu][forward] as f64) / (N as f64) / correction_to_probability;

                        if rng.gen::<f64>() < p_nu{
                            lattice.sites.ks[site_index][nu][forward] -= 1;
                            return Some((site_index, (new_index_from_move(&lattice.shape, &lattice.strides, site_index, nu, forward), nu, forward)));
                        }
                    }
                }
            }
            return None;
        }

        pub(super) fn meson_worm_step2_weight(lattice: &Lattice, params: &WorldParams, (index, direction, forward): (usize, usize, usize)) -> f64 {
            let neighbor_index = new_index_from_move(&lattice.shape, &lattice.strides, index, direction, forward);
            //if lattice.sites.member_of_baryon_loops[neighbor_index] {0.0} else {1.0}
            if lattice.sites.member_of_baryon_loops[neighbor_index] {0.0} else {if direction == 0 {f64::powi(params.gamma,2)} else {1.0}}
        }

        pub(super) fn meson_worm_step2_w_d(lattice: &Lattice, params: &WorldParams, index: usize) -> f64 {
            let mut sum = 0.0;
            for direction in 0..lattice.dimensions{
                for forward in [0,1]{
                    sum += meson_worm_step2_weight(lattice, &params, (index, direction, forward));
                }
            }
            sum
        }

        fn meson_worm_step2(lattice: &mut Lattice, params: &WorldParams, (index, nu, nuforward): (usize, usize, usize), rng: &mut impl Rng) -> (usize, usize, usize){


            let mut correction_to_probability = 1.0;

            let w_d = meson_worm_step2_w_d(lattice, params, index);

            for rho in 0..lattice.dimensions{
                for rhoforward in [0,1]{
                    let p_rho: f64 = meson_worm_step2_weight(lattice, params, (index, rho, rhoforward)) / (w_d * correction_to_probability);

                    if rng.gen::<f64>() < p_rho {
                        lattice.sites.ks[index][nu][1 - nuforward] -= 1;
                        lattice.sites.ks[index][rho][rhoforward] += 1;
                        return (new_index_from_move(&lattice.shape, &lattice.strides, index, rho, rhoforward), rho, rhoforward);
                    }
                    correction_to_probability *= 1.0 - p_rho;
                }
            }
            panic!("meson step 2 not completed");
        }

        fn meson_worm_step3(lattice: &mut Lattice, (index, rho, rhoforward): (usize, usize, usize), starting_index: usize, rng: &mut impl Rng) -> (bool,(usize, usize, usize)){
            let mut correction_to_probability;
            if index == starting_index{
                let p_z: f64 = 1.0 / ((N - lattice.sites.ks[index][rho][1 - rhoforward]) as f64);

                if rng.gen::<f64>() < p_z {
                    lattice.sites.ks[index][rho][1 - rhoforward] += 1;
                    return (true, (0,0,0));
                }
                correction_to_probability = 1.0 - p_z;
            }else{
                correction_to_probability = 1.0;
            }
            for mu in 0..lattice.dimensions{
                for muforward in [0, 1]{


                    let p_mu = (lattice.sites.ks[index][mu][muforward] as f64) / (((N - lattice.sites.ks[index][rho][1 - rhoforward]) as f64) * correction_to_probability);
                    if mu == rho && muforward == 1 - rhoforward{
                        continue
                    }
                    if rng.gen::<f64>() < p_mu {
                        lattice.sites.ks[index][rho][1 - rhoforward] += 1;
                        lattice.sites.ks[index][mu][muforward] -= 1;
                        return (false, (new_index_from_move(&lattice.shape, &lattice.strides, index, mu, muforward), mu, muforward));
                    }
                    correction_to_probability *= 1.0 - p_mu;
                }
            }
            panic!("meson step 3 not completed");
        }


        pub(super) fn baryon_worm(lattice: &mut Lattice, params: &WorldParams, rng: &mut impl Rng){
            let result = baryon_worm_step1(lattice, rng);
            let (starting_index, mut result) = match result {
                Some(x) => x,
                None =>{
                    return;
                },
            };
            loop{
                result = baryon_worm_step2(lattice, &params, result, rng);
                result = baryon_worm_step3(lattice, result, starting_index);
                if result.0 == starting_index {
                    return;
                }
            }
        }

        fn baryon_worm_step1(lattice: &mut Lattice, rng: &mut impl Rng) -> Option<(usize, (usize, usize, usize))>{
            let mut mu = 0;
            let mut muforward = 0;
            let mut index;

            let mut tries = 0;
            loop{
                tries += 1;
                if tries > BARYON_WORM_STEP1_TRIES{
                    return None;
                }
                let mut success = false;
                index = rng.gen_range(0..lattice.sites.len);
                for nu in 0 .. lattice.dimensions {
                    for nuforward in [0, 1]{
                        if lattice.sites.ks[index][nu][nuforward] == 3 {
                            success = true;
                            mu = nu;
                            muforward = nuforward;
                        } else if lattice.sites.bs[index][nu][nuforward] == -1 {
                            success = true;
                            mu = nu;
                            muforward = nuforward;
                        }
                    }
                }
                if success {
                    break;
                }
            }
            if lattice.sites.ks[index][mu][muforward] == 3 { // mesonic site
                lattice.sites.bs[index][mu][muforward] = 1;
                lattice.sites.member_of_baryon_loops[index] = true;
                lattice.sites.ks[index][mu][muforward] = 0;
            }else { // baryonic site
                lattice.sites.bs[index][mu][muforward] = 0;
            }

            return Some((index, (new_index_from_move(&lattice.shape, &lattice.strides, index, mu, muforward), mu, muforward)));
        }

        pub(super) fn baryon_worm_step2_weight(lattice: &Lattice, params: &WorldParams, (index, direction, forward): (usize, usize, usize)) -> f64 {
            let site_index = new_index_from_move(&lattice.shape, &lattice.strides, index, direction, forward);

            let mut saturated_site = false;
            for direction in 0..lattice.dimensions {
                for forward in [0, 1]{
                    if lattice.sites.ks[site_index][direction][forward] == 3 {
                        saturated_site = true;
                    }
                }
            }
            if lattice.sites.member_of_baryon_loops[site_index] || saturated_site {
                if direction != 0 {
                    1.0
                }else {
                    let factor1 = f64::powi(params.gamma, 3);
                    let exponent;
                    if forward == 1 {
                        exponent = 1.0;
                    }else {
                        exponent = -1.0;
                    }
                    factor1 * f64::exp(3.0 * params.a_t * params.mu * exponent)
                }
            }else{
                0.0
            }
        }

        pub(super) fn baryon_worm_step2_w_d(lattice: &Lattice, params: &WorldParams, index: usize) -> f64 {
            let mut sum = 0.0;
            for direction in 0..lattice.dimensions {
                for forward in [0, 1] {
                    sum += baryon_worm_step2_weight(lattice, params, (index, direction, forward));
                }
            }
            sum
        }

        fn baryon_worm_step2(lattice: &mut Lattice, params: &WorldParams, (index, mu, muforward): (usize, usize, usize), rng: &mut impl Rng) -> (usize, usize, usize){
            let mut correction_to_probability = 1.0;
            let w_d = baryon_worm_step2_w_d(lattice, params, index);

            for rho in 0..lattice.dimensions {
                for rhoforward in [0, 1] {
                    let weight = baryon_worm_step2_weight(lattice, params, (index, rho, rhoforward));
                    let p_rho = weight / (w_d * correction_to_probability);

                    if rng.gen::<f64>() < p_rho {
                        lattice.sites.bs[index][mu][1 - muforward] -= 1;
                        lattice.sites.ks[index][mu][1 - muforward] = 0;
                        lattice.sites.bs[index][rho][rhoforward] += 1;
                        if lattice.sites.bs[index][rho][rhoforward] == 0 {
                            lattice.sites.ks[index][rho][rhoforward] = 3;
                            lattice.sites.member_of_baryon_loops[index] = false;
                        }else {
                            lattice.sites.member_of_baryon_loops[index] = true;
                        }
                        return(new_index_from_move(&lattice.shape, &lattice.strides, index, rho, rhoforward), rho, rhoforward);
                    }
                    correction_to_probability *= 1.0 - p_rho;
                }
            }
            panic!("baryon step 2 not complete");
        }

        fn baryon_worm_step3(lattice: &mut Lattice, (index, rho, rhoforward): (usize, usize, usize), starting_index: usize) -> (usize, usize, usize){
            if index == starting_index {
                lattice.sites.bs[index][rho][1 - rhoforward] -= 1;
                if lattice.sites.bs[index][rho][1 - rhoforward] == 0 {
                    lattice.sites.ks[index][rho][1 - rhoforward] = 3;
                    lattice.sites.member_of_baryon_loops[index] = false;
                }
                return (index, 0, 0);
            }
            for mu in 0..lattice.dimensions {
                for muforward in [0, 1] {
                    if lattice.sites.ks[index][mu][muforward] == 3 {
                        lattice.sites.bs[index][rho][1 - rhoforward] -= 1;
                        lattice.sites.bs[index][mu][muforward] = 1;
                        lattice.sites.ks[index][mu][muforward] = 0;
                        lattice.sites.member_of_baryon_loops[index] = true;
                        return (new_index_from_move(&lattice.shape, &lattice.strides, index, mu, muforward), mu, muforward);
                    }
                    if lattice.sites.bs[index][mu][muforward] == -1 {
                        lattice.sites.bs[index][rho][1 - rhoforward] -= 1;
                        lattice.sites.bs[index][mu][muforward] = 0;
                        if lattice.sites.bs[index][rho][1 - rhoforward] == 0 {
                            lattice.sites.ks[index][rho][1 - rhoforward] = 3;
                            lattice.sites.member_of_baryon_loops[index] = false;
                        }
                        return (new_index_from_move(&lattice.shape, &lattice.strides, index, mu, muforward), mu, muforward);
                    }
                }
            }
            panic!("[baryon step 3] site was in no possible state");
        }

        use std::sync::mpsc;
        use crate::{Message, SEND_EVERY};
        pub(super) fn thermalize(lattice: &mut Lattice, iterations: usize, params: &WorldParams, rng: &mut impl Rng, tx: &mpsc::Sender<Message>){
            tx.send(Message::TaskMessage(String::from("thermalizing"))).unwrap();
            for iteration in 0..iterations{
                meson_worm(lattice, &params, rng);
                baryon_worm(lattice, &params, rng);
                if iteration % SEND_EVERY == 0{
                    tx.send(Message::ProgressMessage(iteration as f64 / iterations as f64)).unwrap();
                }

            }
        }
    }

    pub mod observables {
        use super::worm::meson_worm;
        use super::worm::baryon_worm;
        use crate::statistics::get_statistics;
        use rand::prelude::*;
        use super::Lattice;
        use super::init_lattice;
        use super::get_coord_from_index;
        use super::new_index_from_move;
        use super::check_winding;
        use super::worm::thermalize;
        use crate::WorldParams;
        use std::sync::mpsc;
        use crate::Message;
        use crate::SEND_EVERY;

        pub(super) fn sign(lattice: &Lattice) -> isize{ // compare Fromm 2010 below (4.13) with eta from below (2.22)
            let mut result: isize = 1;
            let mut removed = vec![false; lattice.sites.len];

            for mut index in 0..lattice.sites.len{
                if removed[index] || !lattice.sites.member_of_baryon_loops[index] {continue;}
                result *= -1;
                let starting_index = index;
                loop{
                    removed[index] = true;
                    'directions: for direction in 0..lattice.dimensions {
                        for forward in [0, 1] {
                            if lattice.sites.bs[index][direction][forward] == -1 {
                                if direction == 0 && check_winding(&lattice.shape, index, direction, forward) {result *= -1;}
                                index = new_index_from_move(&lattice.shape, &lattice.strides, index, direction, forward);
                                let mut eta = 1;
                                for smaller_direction in 0..direction{
                                    eta *= isize::pow(-1, get_coord_from_index(&lattice.shape, index, smaller_direction) as u32);
                                }
                                result *= eta;
                                result *= forward as isize * 2 - 1;
                                break 'directions;
                            }
                        }
                    }
                    if index == starting_index {break;}
                }
            }
            result
        }

        pub(super) fn sign_average(lattice: &mut Lattice, amount_samples: usize, params: &WorldParams, rng: &mut impl Rng, tx: &mpsc::Sender<Message>) -> (f64, f64, f64){
            tx.send(Message::TaskMessage(String::from("calculating sign average"))).unwrap();
            let mut sign_samples = vec![0.0; amount_samples];
            for sample_index in 0..amount_samples {
                meson_worm(lattice, &params, rng);
                baryon_worm(lattice, &params, rng);
                sign_samples[sample_index] = sign(lattice) as f64;
                if sample_index % SEND_EVERY == 0 {
                    tx.send(Message::ProgressMessage(sample_index as f64 / amount_samples as f64)).unwrap();
                }
            }
            get_statistics(&sign_samples, tx)
        }

        pub(super) fn winding_number(lattice: &Lattice) -> isize{
            let mut winding_number = 0;
            for index in 0..lattice.sites.len{
                if get_coord_from_index(&lattice.shape, index, 0) == 0{ // cut a plane through x_0 = 0 and count piercings
                    winding_number -= lattice.sites.bs[index][0][0];
                }
            }
            winding_number
        }

        pub(super) fn winding_density(lattice: &mut Lattice, amount_samples: usize, params: &WorldParams, rng: &mut impl Rng, tx: &mpsc::Sender<Message>) -> (f64, f64, f64){
            tx.send(Message::TaskMessage(String::from("calculating baryon density"))).unwrap();

            let mut winding_samples: Vec<f64> = vec![0.0; amount_samples];

            for sample_index in 0..amount_samples {
                meson_worm(lattice, &params, rng);
                baryon_worm(lattice, &params, rng);
                winding_samples[sample_index] = ((3 * winding_number(lattice) * sign(lattice)) as f64) / lattice.volume as f64;
                if sample_index % SEND_EVERY == 0 {
                    tx.send(Message::ProgressMessage(sample_index as f64 / amount_samples as f64)).unwrap();
                }
            }
            get_statistics(&winding_samples, tx)
        }

        pub(super) fn baryon_density(lattice: &mut Lattice, amount_samples: usize, params: &WorldParams, (sign_average, sign_std, sign_autocorr): &(f64, f64, f64), rng: &mut impl Rng, tx: &mpsc::Sender<Message>) -> (f64, f64, f64) {
            let (winding_density, winding_std, winding_autocorr) = winding_density(lattice, amount_samples, &params, rng, tx);
            (winding_density / sign_average, winding_std + sign_std, winding_autocorr * sign_autocorr)
        }

        pub(super) fn energy_density(lattice: &mut Lattice, amount_samples: usize, params: &WorldParams, (sign_average, sign_std, sign_autocorr): &(f64, f64, f64), rng: &mut impl Rng, tx: &mpsc::Sender<Message>) -> (f64, f64, f64) {
            tx.send(Message::TaskMessage(String::from("calculating energy density"))).unwrap();

            let mut energy_samples = vec![0.0; amount_samples];
            for sample_index in 0..amount_samples {
                meson_worm(lattice, &params, rng);
                baryon_worm(lattice, &params, rng);
                let sign_var = sign(&lattice);
                let mut links =  0;
                for site_index in 0..lattice.sites.len {
                    links += 2 * lattice.sites.ks[site_index][0][1] as isize * sign_var;
                    links += 3 * isize::abs(lattice.sites.bs[site_index][0][1]) as isize * sign_var;
                }
                energy_samples[sample_index] = links as f64 / (lattice.volume * lattice.shape[0]) as f64 / params.gamma;
                if sample_index % SEND_EVERY == 0 {
                    tx.send(Message::ProgressMessage(sample_index as f64 / amount_samples as f64)).unwrap();
                }
            }
            let (link_density, link_std, link_autocorr): (f64, f64, f64) = get_statistics(&energy_samples, tx);
            (link_density / sign_average, link_std + sign_std, link_autocorr * sign_autocorr)
        }

        pub(crate) struct RunSpecifications {
            pub lattice_shape: Vec<usize>,
            pub gammas: Vec<f64>,
            pub mus: Vec<f64>,
            pub amount_samples: usize,
            pub thermalization_iterations: usize,
        }


        //                                                                                              gamma     mu    avg, std, auto
        pub(crate) fn calculate_observables(specs: RunSpecifications, tx: &mpsc::Sender<Message>){
            tx.send(Message::TaskMessage(String::from("setting up lattice"))).unwrap();
            let mut lattice = init_lattice(specs.lattice_shape);
            let mut rng = rand_pcg::Pcg64Mcg::from_entropy();
            for gamma in specs.gammas{
                for &mu in &specs.mus{
                    tx.send(Message::ParameterMessage(gamma, mu)).unwrap();
                    let params = WorldParams{
                        gamma,
                        mu,
                        a_t: 1.0,
                    };
                    thermalize(&mut lattice, specs.thermalization_iterations, &params, &mut rng, tx);
                    let sign_result = sign_average(&mut lattice, specs.amount_samples, &params, &mut rng, tx);
                    let observables = [baryon_density(&mut lattice, specs.amount_samples, &params, &sign_result, &mut rng,  tx),
                                        energy_density(&mut lattice, specs.amount_samples, &params, &sign_result, &mut rng, tx),
                                        sign_result];
                    tx.send(Message::ResultMessage(gamma, mu, observables)).unwrap();
                }
            }
        }
    }

    #[cfg(test)]
    pub mod tests{
        use super::worm::*;
        use rand::prelude::*;
        use crate::WorldParams;
        use super::Lattice;
        use crate::N;
        use super::init_lattice;
        use super::new_index_from_move;

        #[test]
        fn validate_lattice_creation(){
            let lattice = init_lattice(vec!(4, 4, 4, 4));
            assert!(validate_lattice(&lattice));
        }

        fn validate_lattice(lattice: &Lattice) -> bool {
            for index in 0..lattice.sites.len {
                let mut meson_sum = 0;
                let mut baryon_sum = 0;
                for direction in 0..lattice.dimensions{
                    for forward in [0, 1]{
                        meson_sum += lattice.sites.ks[index][direction][forward];
                        baryon_sum += lattice.sites.bs[index][direction][forward];
                        let neighbor_index = new_index_from_move(&lattice.shape, &lattice.strides, index, direction, forward);
                        if lattice.sites.ks[neighbor_index][direction][1 - forward] != lattice.sites.ks[index][direction][forward] {
                            println!("site ks out of sync at index {}, direction {}, forward {}", index, direction, forward);
                            return false;
                        }
                        if lattice.sites.bs[neighbor_index][direction][1 - forward] != -lattice.sites.bs[index][direction][forward] {
                            println!("site bs out of sync");
                            println!("neighbor b: {}, site b: {}", lattice.sites.bs[neighbor_index][direction][1 - forward], lattice.sites.bs[index][direction][forward]);
                            return false;
                        }
                    }
                }
                if meson_sum != N && !lattice.sites.member_of_baryon_loops[index]{
                    println!("site N wrong: N={:?}", lattice.sites.ks[index]);
                    return false;
                }
                if baryon_sum != 0{
                    println!("site baryon_sum out of wrong");
                    return false;
                }
                if meson_sum != 0 && lattice.sites.member_of_baryon_loops[index]{
                    println!("site ks not 0, but member_of_baryon_loop");
                    return false;
                }
            }
            true
        }


        #[test]
        fn validate_meson_worm(){
            let mut lattice = init_lattice(vec!(4, 4, 4, 4));
            let params = WorldParams{
                gamma: 1.0,
                mu: 0.5,
                a_t: 1.0,
            };
            let mut rng = rand_pcg::Pcg64Mcg::seed_from_u64(2);
            for _ in 0..1_000{
                meson_worm(&mut lattice, &params, &mut rng);
            }
            assert!(validate_lattice(&lattice));
        }

        #[test]
        fn validate_baryon_worm(){
            let mut lattice = init_lattice(vec!(4, 4, 4, 4));
            let params = WorldParams{
                gamma: 1.0,
                mu: 0.5,
                a_t: 1.0,
            };
            let mut rng = rand_pcg::Pcg64Mcg::seed_from_u64(2);
            for _ in 0..1_000{
                meson_worm(&mut lattice, &params, &mut rng);
                baryon_worm(&mut lattice, &params, &mut rng);
            }
            assert!(validate_lattice(&lattice));
        }


        #[test]
        fn validate_sign(){
            use super::LatticeSites;
            use super::observables::sign;
            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true; 4],
                ks: vec![vec![[0; 2]; 2]; 4],
                bs: vec![vec![[-1, 1], [0, 0]]; 4],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };
            assert!(validate_lattice(&lattice));
            assert!(sign(&lattice) == 1);

            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true, true, false, false],
                ks: vec![vec![[0, 0], [0, 0]], vec![[0, 0], [0, 0]], vec![[0, 3], [0, 0]], vec![[3, 0], [0, 0]]],
                bs: vec![vec![[-1, 1], [0, 0]], vec![[-1, 1], [0, 0]], vec![[0, 0], [0, 0]], vec![[0, 0], [0, 0]]],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };


            assert!(validate_lattice(&lattice));
            assert!(sign(&lattice) == 1);


            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true, true, true, true],
                ks: vec![vec![[0, 0], [0, 0]], vec![[0, 0], [0, 0]], vec![[0, 0], [0, 0]], vec![[0, 0], [0, 0]]],
                bs: vec![vec![[-1, 0], [0, 1]], vec![[0, 1], [-1, 0]], vec![[0, 1], [-1, 0]], vec![[-1, 0], [0, 1]]],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };


            assert!(validate_lattice(&lattice));
            assert!(sign(&lattice) == -1);
        }

        #[test]
        pub(crate) fn validate_winding_number(){
            use super::LatticeSites;
            use super::observables::winding_number;
            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true; 4],
                ks: vec![vec![[0; 2]; 2]; 4],
                bs: vec![vec![[1, -1], [0, 0]]; 4],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };
            let winding_result = winding_number(&lattice);
            assert!(validate_lattice(&lattice));
            assert!(winding_result == -2);


            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true; 4],
                ks: vec![vec![[0; 2]; 2]; 4],
                bs: vec![vec![[1, -1], [0, 0]], vec![[1, -1], [0, 0]], vec![[-1, 1], [0, 0]], vec![[-1, 1], [0, 0]]],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };
            let winding_result = winding_number(&lattice);
            assert!(validate_lattice(&lattice));
            assert!(winding_result == 0);
        }

        #[test]
        fn validate_meson_worm_step2_w_d(){
            use super::worm::meson_worm_step2_w_d;
            let params = WorldParams{
                gamma: 1.0,
                mu: 0.5,
                a_t: 0.0,
            };
            let lattice = init_lattice(vec![2,2]);
            let result = meson_worm_step2_w_d(&lattice, &params, 0);
            println!("w_d={}", result);
            assert!(result == 4.0);
        }

        #[test]
        fn validate_meson_worm_step2_weight(){
            use super::worm::meson_worm_step2_weight;
            let params = WorldParams{
                gamma: 1.0,
                mu: 0.5,
                a_t: 0.0,
            };
            let lattice = init_lattice(vec![2,2]);
            let result = meson_worm_step2_weight(&lattice, &params, (0, 0, 1));
            println!("weight = {}", result);
            assert!(result == 1.0);
        }

        #[test]
        fn validate_baryon_worm_step2_weight(){
            use super::LatticeSites;
            let sites = LatticeSites{
                len: 4,
                member_of_baryon_loops: vec![true; 4],
                ks: vec![vec![[0; 2]; 2]; 4],
                bs: vec![vec![[1, -1], [0, 0]]; 4],
            };
            let lattice = Lattice{
                dimensions: 2,
                shape: vec![2, 2],
                strides: vec![1, 2, 4],
                volume: 2,
                sites,
            };
            let params = WorldParams{
                gamma: 1.0,
                mu: 0.5,
                a_t: 1.0,
            };
            let result = baryon_worm_step2_weight(&lattice, &params, (0, 0, 1));
            println!("baryon weight = {}", result);
            assert!(f64::abs(result - 4.48169) < 0.01);
        }
    }
}

mod statistics{
    use crate::Message;
    use std::sync::mpsc;
    use crate::SEND_EVERY;


    pub(crate) fn get_statistics(samples: &Vec<f64>, tx: &mpsc::Sender<Message>) -> (f64, f64, f64){
        let (average, standard_deviation) = basic_analysis(samples, tx);
        let autocorrelation = autocorrelation(samples, average, tx);
        (average, standard_deviation * f64::sqrt(2.0 * autocorrelation) / f64::sqrt(samples.len() as f64), autocorrelation)
    }


    fn basic_analysis(samples: &Vec<f64>, tx: &mpsc::Sender<Message>) -> (f64, f64) {
        let mut sum = 0.0;
        for sample in samples {
            sum += sample;
        }
        let average = sum / samples.len() as f64;

        let mut deviation_sum = 0.0;

        tx.send(Message::TaskMessage(String::from("calculating variance"))).unwrap();
        for (index, sample) in samples.iter().enumerate(){
            deviation_sum += f64::powi(average - sample, 2);
            if index % SEND_EVERY == 0 {
                tx.send(Message::ProgressMessage(index as f64 / samples.len() as f64)).unwrap();
            }
        }

        let variance = deviation_sum / samples.len() as f64;

        (average, f64::sqrt(variance))

    }

    fn autocorrelation(samples: &Vec<f64>, average: f64, tx: &mpsc::Sender<Message>) -> f64 {
        tx.send(Message::TaskMessage(String::from("autocorrelation calculation"))).unwrap();
        let mut result = 0.5;
        let length = samples.len();
        let inv_length = 1.0 / length as f64;
        let mut covs = vec![0.0; length];
        tx.send(Message::TaskMessage(String::from("calculating autocovariances"))).unwrap();
        for t in 0..length{
            for i in 0..(length - t){
                covs[t] += (samples[i] - average) * (samples[i + t] - average) * inv_length;
            }
            if t % SEND_EVERY == 0 {
                tx.send(Message::ProgressMessage(t as f64 / length as f64)).unwrap();
            }
        }

        tx.send(Message::TaskMessage(String::from("calculating autocorrelation times"))).unwrap();
        for t in 1..length{
            let summand = (1.0 - t as f64 * inv_length) * covs[t] / covs[0];
            if summand < 0.0 {break;}
            result += summand;
            if t % SEND_EVERY == 0 {
                tx.send(Message::ProgressMessage((t - 1) as f64 / (length - 1) as f64)).unwrap();
            }
        }
        result
    }

}

mod file_managment{
    use std::path::Path;
    use std::fs::File;
    use std::thread;
    use std::sync::mpsc;
    use crate::lattice::observables::calculate_observables;
    use crate::Message;
    use terminal_size;
    use std::io::{self, Write};
    extern crate termion;
    use termion::{color, style};

    struct ThreadStatus {
        writer: csv::Writer<File>,
        task: String,
        progress: f64,
        current_params: (f64, f64),
        last_result: (f64, f64, [(f64, f64, f64); 3]),
        done: bool,
    }

    pub(crate) use crate::lattice::observables::RunSpecifications;
    pub(crate) fn simulate_run(spec_list: Vec<(String, RunSpecifications)>){
        let mut handles = Vec::new();
        let mut channels = Vec::new();
        let mut statuses = Vec::new();
        for (filename, spec) in spec_list{

            let (tx, rx) = mpsc::channel();
            channels.push(rx);

            let mut path = String::from("out/");
            path.push_str(&filename);
            let path = Path::new(&path);
            let file = match File::create(&path){
                Err(why) => panic!("could not open file: {}", why),
                Ok(file) => file,
            };

            let mut wtr = csv::Writer::from_writer(file);
            let write_result = wtr.write_record(&["mu", "gamma", "baryonDensityAverage", "baryonDensityError", "baryonAutocorr", "energyDensityAverage", "energyDensityError", "signAverage", "signError"]);

            match write_result{
                Ok(_) => (),
                Err(why) => println!("could not write to file {:?}, because {}", path, why),
            };


            let handle = thread::spawn(move || {
                calculate_observables(spec, &tx);
                tx.send(Message::ProgressMessage(1.0)).unwrap();
                tx.send(Message::DoneMessage()).unwrap();
            });
            statuses.push(ThreadStatus{
                    writer: wtr,
                    task: String::from("initializing"),
                    progress: 0.0,
                    current_params: (0.0, 0.0),
                    last_result: (0.0, 0.0, [(0.0, 0.0, 0.0); 3]),
                    done: false,
                }
            );
            handles.push(handle);
        }

        print!("\x1b[2J"); // clear screen
        let mut done_list = vec![false; handles.len()];
        loop{
            std::thread::sleep(std::time::Duration::from_millis(10));
            for (rx, mut status) in (&channels).iter().zip((statuses).iter_mut()){
                let received = match rx.try_recv() {
                    Ok(msg) => msg,
                    Err(_) => {continue;},
                };
                match received {
                    Message::ParameterMessage(gamma, mu) => {
                        status.current_params = (gamma, mu);
                    }
                    Message::TaskMessage(msg) => {
                        status.task = msg;
                    }
                    Message::ProgressMessage(progress) => {
                        status.progress = progress;
                    }
                    Message::ResultMessage(gamma, mu, values) => {
                        status.last_result = (gamma, mu, values);

                        let write_result = status.writer.write_record(&[mu.to_string(), gamma.to_string(), values[0].0.to_string(), values[0].1.to_string(), values[0].2.to_string(), values[1].0.to_string(), values[1].1.to_string(), values[2].0.to_string(), values[2].1.to_string()]);

                        match write_result{
                            Ok(_) => (),
                            Err(why) => panic!("could not write to file, because {}", why),
                        };
                    }
                    Message::DoneMessage() => {
                        status.done = true;
                        print!("\x1b[2K");
                        println!("done");
                        panic!("done");
                    }
                };
            }
            let mut to_remove = Vec::new();
            for (index, status) in statuses.iter().enumerate() {
                if status.done {to_remove.push(index);}
            }
            for index_to_remove in to_remove {
                done_list.remove(index_to_remove);
                statuses.remove(index_to_remove);
                channels.remove(index_to_remove);
            }
            print!("\x1b[2J"); // clear screen
            for (index, status) in statuses.iter().enumerate() {

                print!("\x1b[{};0f", index * 6 + 1); // set cursor position
                let (gamma, mu) = status.current_params;
                println!("{}{}gamma={}, mu={}{}",style::Bold, color::Fg(color::Cyan), gamma, mu, style::Reset);
                println!("{}{}{}", color::Fg(color::LightRed), status.task, style::Reset);

                let width = match terminal_size::terminal_size() {
                    Some((terminal_size::Width(x), _)) => x,
                    None => {
                        println!("could not find terminal size");
                        continue;
                    },
                };

                let bar_size = ((width - 2) as f64 * status.progress) as usize;
                println!("{}[{}{}{}]{}", color::Fg(color::Green), std::iter::repeat('#').take(bar_size).collect::<String>(), color::Fg(color::Red), std::iter::repeat('-').take((width - 2) as usize - bar_size).collect::<String>(), style::Reset);
                println!("{}progress: {:.1}%{}", color::Fg(color::Green), status.progress * 100.0, style::Reset);
                let (gamma, mu, values) = status.last_result;
                print!("{}last result for gamma={:.3}, mu={:.3}: baryon_density={:.3}±{:.3}, energy_density={:.3}±{:.3}, avg_sign={:.3}±{:.3}, autocorrelation={:.3}{}", color::Fg(color::Cyan), gamma, mu, values[0].0.to_string(), values[0].1.to_string(), values[1].0.to_string(), values[1].1.to_string(), values[2].0.to_string(), values[2].1.to_string(), values[0].2.to_string(), style::Reset);
            }
            io::stdout().flush().unwrap();
            if channels.len() == 0{
                break;
            }
        }
    }
}


fn main() {
    use file_managment::simulate_run;

    let mut mu_list = Vec::new();

    for mu in 0..15 {
        let mu = mu as f64 / 10.0;
        mu_list.push(mu);
    }


    let mut spec_list = Vec::new();

    for dimensionality in 2..5{
        for extent in [2,]{
            let mut shape = Vec::new();
            let mut filename = String::from("values_");
            for _ in 0..dimensionality{
                filename += &extent.to_string();
                filename.push_str("x");
                shape.push(extent);
            }
            filename.pop();
            filename.push_str(".csv");
            spec_list.push(
                (   filename,
                    lattice::observables::RunSpecifications{
                    lattice_shape: shape,
                    gammas: vec![0.5, 1.0, 1.5],
                    mus: mu_list.clone(),
                    amount_samples: 1_000,
                    thermalization_iterations: 1000,
                })
            );
        }
    }


    //let spec_list = vec![(String::from("values_2x2.csv"), run1)];


    simulate_run(spec_list);
}
